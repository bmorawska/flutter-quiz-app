import 'package:flutter/material.dart';

class ColoredText extends StatelessWidget {
  final int colorIdx;

  ColoredText(@required this.colorIdx);

  Color idxToColor() {
    switch (colorIdx) {
      case 0:
        return Colors.red;
      case 1:
        return Colors.blue;
      case 2:
        return Colors.orange;
      case 3:
        return Colors.black;
    }

    return null;
  }

  String idxToString() {
    switch (colorIdx) {
      case 0:
        return 'CZERWONY';
      case 1:
        return 'NIEBIESKI';
      case 2:
        return 'ŻÓŁTY';
      case 3:
        return 'BIAŁY';
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      idxToString(),
      textAlign: TextAlign.justify,
      style: TextStyle(
        color: idxToColor(),
        fontSize: 36,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
