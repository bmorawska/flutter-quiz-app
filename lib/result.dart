import 'package:flutter/material.dart';

import './dataBase.dart';
import './coloredText.dart';

class Result extends StatelessWidget {
  final List<int> resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  int maxValue() {
    int maxIdx = 0;
    int max = resultScore[maxIdx];
    for (int i = 1; i < resultScore.length; i++) {
      if (resultScore[i] > max) {
        max = resultScore[i];
        maxIdx = i;
      }
    }

    return maxIdx;
  }

  String get resultPhrase {
    int idx = maxValue();

    switch (idx) {
      case 0:
        return red;
      case 1:
        return blue;
      case 2:
        return yellow;
      case 3:
        return white;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          ColoredText(maxValue()),
          Text(
            resultPhrase,
            style: TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.justify,
          ),
          FlatButton(
            child: Text('Restartuj'),
            onPressed: resetHandler,
            textColor: Colors.blue,
          ),
        ],
      ),
    );
  }
}
