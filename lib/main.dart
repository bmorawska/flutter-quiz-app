import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';
import './dataBase.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = [0, 0, 0, 0];
  bool isStartPage = true;

  void _answerQuestion(String index) {
    List<int> scores;

    switch (index) {
      case 'a':
        scores = [1, 0, 0, 0];
        break;
      case 'b':
        scores = [0, 1, 0, 0];
        break;
      case 'c':
        scores = [0, 0, 1, 0];
        break;
      case 'd':
        scores = [0, 0, 0, 1];
        break;
    }

    for (int i = 0; i < _totalScore.length; i++) {
      _totalScore[i] += scores[i];
    }

    setState(() {
      _questionIndex++;
      isStartPage = false;
    });
    print(_totalScore);
  }

  void _resetQuiz() {
    setState(() {
      _totalScore = [0, 0, 0, 0];
      _questionIndex = 0;
      isStartPage = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('Test Harmana'),
      ),
      body: _questionIndex < questionsFromBase.length
          ? Quiz(
              answerQuestion: _answerQuestion,
              questions: questionsFromBase,
              questionIndex: _questionIndex,
            )
          : Result(_totalScore, _resetQuiz),
    ));
  }
}
