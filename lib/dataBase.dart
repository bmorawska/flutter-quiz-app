final questionsFromBase = const [
  {
    'number': 1,
    'text':
        'Odpowiedz na poniższe 45 pytań. Zaznacz odpowiedź, która najlepiej do Ciebie pasuje. Wybierz tylko jedną z każdej grupy',
    'answers': [
      {'text': 'Nie zmieniam poglądów.', 'index': 'a'},
      {'text': 'Jestem opiekuńczy.', 'index': 'b'},
      {'text': 'Jestem pomysłowy.', 'index': 'c'},
      {'text': 'Lubię rozrywki.', 'index': 'd'}
    ]
  },
  {
    'number': 2,
    'text': '',
    'answers': [
      {'text': 'Lubię mieć władzę.', 'index': 'a'},
      {'text': 'Jestem perfekcjonistą.', 'index': 'b'},
      {'text': 'Jestem niezdecydowany.', 'index': 'c'},
      {'text': 'Jestem egocentryczny.', 'index': 'd'}
    ]
  },
  {
    'number': 3,
    'text': '',
    'answers': [
      {'text': 'Jestem dominujący.', 'index': 'a'},
      {'text': 'Jestem życzliwy.', 'index': 'b'},
      {'text': 'Jestem tolerancyjny.', 'index': 'c'},
      {'text': 'Jestem entuzjastyczny.', 'index': 'd'}
    ]
  },
  {
    'number': 4,
    'text': '',
    'answers': [
      {'text': 'Jestem samodzielny.', 'index': 'a'},
      {'text': 'Jestem podejrzliwy.', 'index': 'b'},
      {'text': 'Jestem niepewny siebie.', 'index': 'c'},
      {'text': 'Jestem naiwny.', 'index': 'd'}
    ]
  },
  {
    'number': 5,
    'text': '',
    'answers': [
      {'text': 'Szybko podejmuję decyzje.', 'index': 'a'},
      {'text': 'Jestem lojalny.', 'index': 'b'},
      {'text': 'Jestem zadowolony.', 'index': 'c'},
      {'text': 'Jestem wesoły.', 'index': 'd'}
    ]
  },
  {
    'number': 6,
    'text': '',
    'answers': [
      {'text': 'Jestem arogancki.', 'index': 'a'},
      {'text': 'Często się martwię.', 'index': 'b'},
      {'text': 'Jestem uparty.', 'index': 'c'},
      {'text': 'Jestem niestały w uczuciach.', 'index': 'd'}
    ]
  },
  {
    'number': 7,
    'text': '',
    'answers': [
      {'text': 'Jestem asertywny.', 'index': 'a'},
      {'text': 'Można na mnie polegać.', 'index': 'b'},
      {'text': 'Jestem miły.', 'index': 'c'},
      {'text': 'Jestem towarzyski.', 'index': 'd'}
    ]
  },
  {
    'number': 8,
    'text': '',
    'answers': [
      {'text': 'Jestem apodyktyczny.', 'index': 'a'},
      {'text': 'Jestem samokrytyczny.', 'index': 'b'},
      {'text': 'Jestem niechętny do działania.', 'index': 'c'},
      {'text': 'Lubię prowokować.', 'index': 'd'}
    ]
  },
  {
    'number': 9,
    'text': '',
    'answers': [
      {'text': 'Lubię działać.', 'index': 'a'},
      {'text': 'Mam naturę analityka.', 'index': 'b'},
      {'text': 'Jestem wyrozumiały.', 'index': 'c'},
      {'text': 'Jestem beztroski.', 'index': 'd'}
    ]
  },
  {
    'number': 10,
    'text': '',
    'answers': [
      {'text': 'Jestem krytyczny.', 'index': 'a'},
      {'text': 'Jestem nadwrażliwy.', 'index': 'b'},
      {'text': 'Jestem nieśmiały.', 'index': 'c'},
      {'text': 'Bywam trudny do zniesienia.', 'index': 'd'}
    ]
  },
  {
    'number': 11,
    'text': '',
    'answers': [
      {'text': 'Jestem stanowczy.', 'index': 'a'},
      {'text': 'Jestem pedantyczny.', 'index': 'b'},
      {'text': 'Uważnie słucham.', 'index': 'c'},
      {'text': 'Lubię przyjęcia.', 'index': 'd'}
    ]
  },
  {
    'number': 12,
    'text': '',
    'answers': [
      {'text': 'Jestem wymagający.', 'index': 'a'},
      {'text': 'Nie wybaczam.', 'index': 'b'},
      {'text': 'Brak mi motywacji.', 'index': 'c'},
      {'text': 'Jestem próżny', 'index': 'd'}
    ]
  },
  {
    'number': 13,
    'text': '',
    'answers': [
      {'text': 'Jestem odpowiedzialny.', 'index': 'a'},
      {'text': 'Jestem idealistą.', 'index': 'b'},
      {'text': 'Jestem delikatny.', 'index': 'c'},
      {'text': 'Jestem szczęśliwy.', 'index': 'd'}
    ]
  },
  {
    'number': 14,
    'text': '',
    'answers': [
      {'text': 'Jestem niecierpliwy.', 'index': 'a'},
      {'text': 'Miewam zmienne nastroje.', 'index': 'b'},
      {'text': 'Jestem bierny.', 'index': 'c'},
      {'text': 'Jestem impulsywny.', 'index': 'd'}
    ]
  },
  {
    'number': 15,
    'text': '',
    'answers': [
      {'text': 'Mam silną wolę.', 'index': 'a'},
      {'text': 'Szanuję innych.', 'index': 'b'},
      {'text': 'Jestem cierpliwy.', 'index': 'c'},
      {'text': 'Lubię żartować.', 'index': 'd'}
    ]
  },
  {
    'number': 16,
    'text': '',
    'answers': [
      {'text': 'Lubię dyskutować.', 'index': 'a'},
      {'text': 'Jestem marzycielem.', 'index': 'b'},
      {'text': 'Brak mi celu w życiu.', 'index': 'c'},
      {'text': 'Przerywam innym.', 'index': 'd'}
    ]
  },
  {
    'number': 17,
    'text': '',
    'answers': [
      {'text': 'Jestem niezależny.', 'index': 'a'},
      {'text': 'Można mi zaufać.', 'index': 'b'},
      {'text': 'Jestem zrównoważony.', 'index': 'c'},
      {'text': 'Jestem ufny.', 'index': 'd'}
    ]
  },
  {
    'number': 18,
    'text': '',
    'answers': [
      {'text': 'Jestem agresywny.', 'index': 'a'},
      {'text': 'Często wpadam w depresję.', 'index': 'b'},
      {'text': 'Bywam wieloznaczny.', 'index': 'c'},
      {'text': 'Jestem zapominalski.', 'index': 'd'}
    ]
  },
  {
    'number': 19,
    'text': '',
    'answers': [
      {'text': 'Jestem władczy.', 'index': 'a'},
      {'text': 'Jestem rozważny.', 'index': 'b'},
      {'text': 'Jestem taktowny.', 'index': 'c'},
      {'text': 'Jestem optymistą.', 'index': 'd'}
    ]
  },
  {
    'number': 20,
    'text': '',
    'answers': [
      {'text': 'Jestem mało wrażliwy.', 'index': 'a'},
      {'text': 'Często osądzam ludzi.', 'index': 'b'},
      {'text': 'Jestem nudny.', 'index': 'c'},
      {'text': 'Jestem niezdyscyplinowany.', 'index': 'd'}
    ]
  },
  {
    'number': 21,
    'text': '',
    'answers': [
      {'text': 'Myślę logicznie.', 'index': 'a'},
      {'text': 'Ulegam emocjom.', 'index': 'b'},
      {'text': 'Jestem ustępliwy.', 'index': 'c'},
      {'text': 'Jestem lubiany.', 'index': 'd'}
    ]
  },
  {
    'number': 22,
    'text': '',
    'answers': [
      {'text': 'Mam zawsze rację.', 'index': 'a'},
      {'text': 'Często mam poczucie winy.', 'index': 'b'},
      {'text': 'Jestem mało entuzjastyczny.', 'index': 'c'},
      {'text': 'Jestem mało zaangażowany.', 'index': 'd'}
    ]
  },
  {
    'number': 23,
    'text': '',
    'answers': [
      {'text': 'Jestem pragmatykiem.', 'index': 'a'},
      {'text': 'Jestem kulturalny.', 'index': 'b'},
      {'text': 'Jestem otwarty.', 'index': 'c'},
      {'text': 'Jestem spontaniczny.', 'index': 'd'}
    ]
  },
  {
    'number': 24,
    'text': '',
    'answers': [
      {'text': 'Jestem bezlitosny.', 'index': 'a'},
      {'text': 'Jestem troskliwy.', 'index': 'b'},
      {'text': 'Nie emocjonuję się tym, co robię.', 'index': 'c'},
      {'text': 'Lubię się popisywać.', 'index': 'd'}
    ]
  },
  {
    'number': 25,
    'text': '',
    'answers': [
      {'text': 'Skupiam się na osiąganiu celu.', 'index': 'a'},
      {'text': 'Jestem szczery.', 'index': 'b'},
      {'text': 'Jestem dyplomatą.', 'index': 'c'},
      {'text': 'Jestem energiczny.', 'index': 'd'}
    ]
  },
  {
    'number': 26,
    'text': '',
    'answers': [
      {'text': 'Jestem nietaktowny.', 'index': 'a'},
      {'text': 'Jestem wybredny.', 'index': 'b'},
      {'text': 'Jestem leniwy.', 'index': 'c'},
      {'text': 'Jestem hałaśliwy.', 'index': 'd'}
    ]
  },
  {
    'number': 27,
    'text': '',
    'answers': [
      {'text': 'Jestem bezpośredni.', 'index': 'a'},
      {'text': 'Jestem twórczy.', 'index': 'b'},
      {'text': 'Łatwo się przystosowuję.', 'index': 'c'},
      {'text': 'Żyję na pokaz.', 'index': 'd'}
    ]
  },
  {
    'number': 28,
    'text': '',
    'answers': [
      {'text': 'Jestem wyrachowany.', 'index': 'a'},
      {'text': 'Jestem obłudny.', 'index': 'b'},
      {'text': 'Jestem zakompleksiony.', 'index': 'c'},
      {'text': 'Jestem niezorganizowany.', 'index': 'd'}
    ]
  },
  {
    'number': 29,
    'text': '',
    'answers': [
      {'text': 'Jestem pewny siebie.', 'index': 'a'},
      {'text': 'Jestem zdyscyplinowany.', 'index': 'b'},
      {'text': 'Jestem sympatyczny.', 'index': 'c'},
      {'text': 'Jestem charyzmatyczny.', 'index': 'd'}
    ]
  },
  {
    'number': 30,
    'text': '',
    'answers': [
      {'text': 'Onieśmielam innych.', 'index': 'a'},
      {'text': 'Jestem ostrożny.', 'index': 'b'},
      {'text': 'Jestem nieproduktywny.', 'index': 'c'},
      {'text': 'Unikam konfrontacji.', 'index': 'd'}
    ]
  },
  {
    'number': 31,
    'text':
        'Gdybym starał się o pracę, prawdopodobnie zatrudniono by mnie, bo jestem:',
    'answers': [
      {'text': 'Bezpośredni i mocno angażuję się w to co robię.', 'index': 'a'},
      {'text': 'Rozważny, dokładny i można na mnie polegać.', 'index': 'b'},
      {
        'text': 'Cierpliwy, taktowny i łatwo adaptuję się w nowych sytuacjach.',
        'index': 'c'
      },
      {'text': 'Towarzyski, nonszalancki i pełen zapału.', 'index': 'd'}
    ]
  },
  {
    'number': 32,
    'text': 'Gdy w związku intymnym czuję się zagrożony przez partnera, wtedy:',
    'answers': [
      {'text': 'Odczuwam gniew i reaguję agresywnie.', 'index': 'a'},
      {'text': 'Płaczę, czuję się urażony, planuję zemstę', 'index': 'b'},
      {
        'text':
            'Jestem spokojny, wycofuję się, często powstrzymuję złość, a potem wybucham z powodu byle drobiazgu.',
        'index': 'c'
      },
      {'text': 'Dystansuję się i unikam dalszych konfliktów.', 'index': 'd'}
    ]
  },
  {
    'number': 33,
    'text': 'Życie ma sens tylko wtedy, gdy:',
    'answers': [
      {
        'text': 'Dążę do osiągnięcia wyznaczonego celu i jestem aktywny.',
        'index': 'a'
      },
      {'text': 'Przebywam z ludźmi i mam określony cel.', 'index': 'b'},
      {'text': 'Jest wolne od stresów i napięć.', 'index': 'c'},
      {
        'text': 'Mogę się nim cieszyć i nie mam powodów do zmartwień.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 34,
    'text': 'Jako dziecko byłem:',
    'answers': [
      {'text': 'Uparty, błyskotliwy i/lub agresywny.', 'index': 'a'},
      {
        'text': 'Grzeczny, troskliwy i/lub miałem skłonność do depresji.',
        'index': 'b'
      },
      {'text': 'Cichy, niewymagający i/lub nieśmiały.', 'index': 'c'},
      {'text': 'Gadatliwy, zadowolony i/lub chętny do zabawy.', 'index': 'd'}
    ]
  },
  {
    'number': 35,
    'text': 'Jako osoba dorosła jestem:',
    'answers': [
      {'text': 'Uparty, stanowczy i/lub apodyktyczny.', 'index': 'a'},
      {'text': 'Odpowiedzialny, uczciwy i/lub pamiętliwy.', 'index': 'b'},
      {
        'text': 'Tolerancyjny, zadowolony i/lub brakuje mi motywacji.',
        'index': 'c'
      },
      {
        'text':
            'Charyzmatyczny, pozytywnie nastawiony do życia i/lub bywam nieznośny.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 36,
    'text': 'Jako rodzic jestem:',
    'answers': [
      {'text': 'Wymagający, porywczy i/lub bezkompromisowy.', 'index': 'a'},
      {'text': 'Troskliwy, wrażliwy i/lub krytyczny.', 'index': 'b'},
      {
        'text':
            'Skłonny do ustępstw, na wszystko pozwalam i/lub czuję się przytłoczony.',
        'index': 'c'
      },
      {
        'text': 'Chętny do zabaw, niesystematyczny i/lub nieodpowiedzialny.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 37,
    'text': 'Podczas sprzeczek z przyjaciółmi najczęściej:',
    'answers': [
      {'text': 'Upieram się przy swoim zdaniu.', 'index': 'a'},
      {
        'text':
            'Myślę o zasadach jakie wyznają i zastanawiam się nad tym, co czują.',
        'index': 'b'
      },
      {
        'text': 'Jestem zacięty, czuję się nieswojo i/lub idę na kompromis.',
        'index': 'c'
      },
      {
        'text':
            'Zachowuję się głośno, czuję się nieswojo i/lub idę na kompromis.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 38,
    'text': 'Gdy przyjaciele mają kłopoty jestem:',
    'answers': [
      {
        'text': 'Opiekuńczy, zaradny i łatwo znajduję rozwiązanie problemu.',
        'index': 'a'
      },
      {
        'text':
            'Szczerze zainteresowany, współczuję i jestem lojalny - niezależnie od problemu.',
        'index': 'b'
      },
      {
        'text': 'Cierpliwy, potrafię podnieść na duchu i chętnie słucham.',
        'index': 'c'
      },
      {
        'text':
            'Powściągliwy w wydawaniu opinii, nastawiony optymistycznie i potrafię rozładować napiętą sytuację.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 39,
    'text': 'Podejmując decyzję jestem:',
    'answers': [
      {'text': 'Asertywny, precyzyjny i logiczny.', 'index': 'a'},
      {'text': 'Rozważny, dokładny i ostrożny.', 'index': 'b'},
      {'text': 'Niezdecydowany, nieśmiały i zniechęcony.', 'index': 'c'},
      {'text': 'Impulsywny, niekonsekwentny i mało zaangażowany.', 'index': 'd'}
    ]
  },
  {
    'number': 40,
    'text': 'W obliczu niepowodzeń:',
    'answers': [
      {
        'text':
            'W duchu jestem samokrytyczny, ale głośno bronię swoich racji i nie przyznaję się do winy.',
        'index': 'a'
      },
      {
        'text':
            'Mam poczucie winy, jestem samokrytyczny, mam skłonność do depresji i w nią wpadam.',
        'index': 'b'
      },
      {'text': 'W duchu odczuwam niepewność i strach.', 'index': 'c'},
      {
        'text': 'Jestem zażenowany i nerwowy, staram się uciec od problemu.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 41,
    'text': 'Gdy ktoś mnie urazi:',
    'answers': [
      {
        'text': 'Jestem zdenerwowany i w duchu planuję szybki rewanż.',
        'index': 'a'
      },
      {
        'text':
            'Czuję się głęboko dotknięty i w zasadzie nigdy całkowicie nie wybaczam. Zemsta to za mało.',
        'index': 'b'
      },
      {
        'text':
            'W głębi duszy czuję się zraniony i szukam odwetu i/lub staram się unikać tej osoby.',
        'index': 'c'
      },
      {
        'text':
            'Unikam konfrontacji, uznaję sytuację za niewartą zachodu i/lub szukam pomocy u przyjaciół.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 42,
    'text': 'Praca to:',
    'answers': [
      {'text': 'Najlepszy sposób na życie.', 'index': 'a'},
      {
        'text':
            'Czynność, którą należy wykonywać najlepiej jak się potrafi, lub nie wykonywać wcale. Moje motto to: najpierw obowiązek, potem przyjemność.',
        'index': 'b'
      },
      {
        'text':
            'Działalność pozytywna, jeśli sprawia mi przyjemność i nie mam obowiązku doprowadzenia jej do końca.',
        'index': 'c'
      },
      {
        'text': 'Zło konieczne, zdecydowanie mniej przyjemne od rozrywki.',
        'index': 'd'
      }
    ]
  },
  {
    'number': 43,
    'text': 'W sytuacjach towarzyskich ludzie najczęściej: ',
    'answers': [
      {'text': 'Boją się mnie.', 'index': 'a'},
      {'text': 'Podziwiają mnie.', 'index': 'b'},
      {'text': 'Zajmują się mną.', 'index': 'c'},
      {'text': 'Zazdroszczą mi.', 'index': 'd'}
    ]
  },
  {
    'number': 44,
    'text': 'W związkach intymnych najbardziej zależy mi, by być:',
    'answers': [
      {'text': 'Aprobowanym i postępować słusznie.', 'index': 'a'},
      {
        'text': 'Rozumianym, docenianym i bliskim drugiej osobie.',
        'index': 'b'
      },
      {'text': 'Szanowanym, tolerancyjnym i zgodnym.', 'index': 'c'},
      {'text': 'Docenianym, wolnym i dobrze się bawić.', 'index': 'd'}
    ]
  },
  {
    'number': 45,
    'text': 'By czuć się dobrze, potrzebuję:',
    'answers': [
      {'text': 'Przywództwa, przygód, działania.', 'index': 'a'},
      {'text': 'Bezpieczeństwa, pracy twórczej, celu.', 'index': 'b'},
      {'text': 'Akceptacji i bezpieczeństwa.', 'index': 'c'},
      {
        'text':
            'Rozrywki, pracy sprwiającej przyjemność i towarzystwa innych ludzi.',
        'index': 'd'
      }
    ]
  },
];

const String red =
    '„Czerwony” to silna, zdecydowana osobowość, to przywódca. Osobowość ta jest bardzo asertywna i będzie liderem grupy, a dzięki umiejętnościom w zakresie dyscypliny i zarządzania często osiągają bardzo dobre wyniki zawodowe. Jednak prowadzi to również do ich negatywnych aspektów: czerwoni muszą mieć rację i często są zbyt krytyczni wobec innych. Nie oznacza to, że „czerwoni” są z natury źli, są kontrolerami i lubią mieć rację i dominować. Czerwony musi być logiczny i praktyczny, przy czym czasami bywa zamknięty w sobie i trudny w kontaktach międzyludzkich.';

const String blue =
    'Niebieski to analityk i organizator, a najważniejszym jest dla niego zdobywanie informacji. To styl emocjonalny, skupiony na kontaktach z innymi ludźmi. Niebiescy to ludzie, którymi lubimy się otaczać, a oni sami do rozwoju potrzebują akceptacji i „bycia lubianym”. Niebieski jest uprzejmy, łagodny, skłonny do współpracy w zespole. Doskonale sprawdza się, jako negocjator godzący sprzeczne interesy zwaśnionych stron. To także utalentowany analityk, bystry obserwator. Najwyżej w hierarchii wartości stawia rodzinę i przyjaciół.';

const String yellow =
    'Radośni, szczęśliwi ludzie w życiu są postrzegani jako kolor żółty. Osoby te są zawsze pozytywne i gotowe na to, co przynosi im życie. Spontaniczna natura zapewnia, że ​​żółci są w stanie w pełni wykorzystać życie i poznać nowych przyjaciół, ale często są zbyt zajęci sobą, aby utrzymać trwałe przyjaźnie lub relacje. Żółci są bardzo kreatywni, nie boją się zmian. To innowatorzy otwarci na nowe doświadczenia.';

const String white =
    'Biali to wizjonerzy i budowniczy. Kolor biały zawsze przynosi spokój umysłowi człowieka i tak samo jest z tą osobowością. Biali są cierpliwi i uprzejmi. Są to przyjaciele, których pragnie każda osoba. Są troskliwi, rozważni i nie osądzają. Będą słuchać godzinami i nie będą chcieli niczego w zamian; będą dobrym przyjacielem. Jednak osoby te padną ofiarą własnych wewnętrznych zmagań, gdy chcą zachować spokój. Nie chcą pracować dla kogoś innego i często nie są zadowoleni ze swoich osiągnięć. Biali są mało skupieni na detalach, zazwyczaj widzą tylko “szeroki plan”. Mają bardzo rozwiniętą intuicję. Są komunikatywni i lubiani w grupie i często dzięki nim ta grupa rozwija swoje pomysły, plany i idee.';
